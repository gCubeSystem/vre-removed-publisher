# VRE Removed Publisher Service

This service allows any client to publish on the GRSF Catalogue.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation


## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Biniam Abraha** 
* **Marco Procaccini** ([ORCID](https://orcid.org/0000-0002-9719-2672)) - [IGG-CNR Group])

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?

    @software{gcat,
		author		= {{Biniam Abraha}, {Marco Procaccini}},
		title		= {VREs Removed Publisher Service},
		abstract	= {VREs Removed Publisher Service Publisher Service},
		keywords	= {VREs, D4Science, gCube}
	}

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

