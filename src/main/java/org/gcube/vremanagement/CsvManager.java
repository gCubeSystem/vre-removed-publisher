package org.gcube.vremanagement;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

public class CsvManager {
	private String csvFilePath;
	private HashMap<String, Gateway> gatewayMap;

	public CsvManager(String csvFilePath) {
		this.csvFilePath = csvFilePath;
		this.gatewayMap = new HashMap<>();
	}


	public HashMap<String, Gateway> parseCsvToMap(String csvFilePath) {
		HashMap<String, Gateway> csvMap = new HashMap<>();
		try {
			FileReader filereader = new FileReader(csvFilePath);
			CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build();

			List<String[]> allData = csvReader.readAll();

			for (String[] row : allData) {
				XmlManager xmlManager = new XmlManager();
				String gatewayName = row[5];
				String csvgatewayName = xmlManager.extractGatewayName(gatewayName)+ " Gateway";
				String description = row[10];
				String voName = row[4];
				String vreName = row[3];
				String manager = row[9];
				String startDate = row[7];
				String endDate = row[8];


				Gateway gateway = csvMap.getOrDefault(csvgatewayName.toLowerCase(), new Gateway(csvgatewayName, description));
				Vos vos = gateway.getVosByName(voName);
				if (vos == null) {
					vos = new Vos(voName, voName, voName);
					gateway.addVos(vos);
				}

				Vres vres = vos.getVresByName(vreName);
				if (vres == null) {
					vres = new Vres(vreName, vreName, description, manager, startDate, endDate);
					vos.addVres(vres);
				}

				csvMap.put(csvgatewayName.toLowerCase(), gateway);
			} 


			csvReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return csvMap;
	} 

	private String extractGatewayName(String gateways) {
		if (gateways.startsWith("https://")) {
			return gateways.substring(8, gateways.indexOf("."));
		} else if (gateways.startsWith("http://")) {
			return gateways.substring(7, gateways.indexOf("."));
		} else {
			return gateways; // fallback
		}
	}
}


