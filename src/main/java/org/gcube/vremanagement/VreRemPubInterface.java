package org.gcube.vremanagement;

import java.util.ArrayList;
import java.util.HashMap;

public interface VreRemPubInterface {

	HashMap<String, Gateway> parse(); //method used to parse the file (e.g., xml or csv) in a data structure
	void update(); //method used to update the file (e.g., xml or csv)
	//add here other common methods needed by both Csv and Xml Manager
	void find();
	void read();
	void write();
	}
