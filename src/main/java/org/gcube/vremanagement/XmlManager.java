package org.gcube.vremanagement;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class XmlManager implements VreRemPubInterface {
	private String filePath;
	private HashMap<String, Gateway> gatewaysMap;

	public XmlManager(String filePath) {
		this.filePath = filePath;
		this.gatewaysMap = new HashMap<>();
	}

	public XmlManager() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public HashMap<String, Gateway> parse() {
		try {
			File xmlFile = new File(filePath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			NodeList gatewayNodes = doc.getElementsByTagName("gateways");

			NodeList gatewayList = gatewayNodes.item(0).getChildNodes();

			for (int i = 0; i < gatewayList.getLength(); i++) {
				Node gatewayNode = gatewayList.item(i);
				if (gatewayNode.getNodeType() == Node.ELEMENT_NODE) {
					Element gatewayElement = (Element) gatewayNode;
					String gatewayName = gatewayElement.getElementsByTagName("name").item(0).getTextContent().trim();
					String gatewayDescription = gatewayElement.getElementsByTagName("description").item(0)
							.getTextContent().trim();

					Gateway gateway = new Gateway(gatewayName, gatewayDescription);

					// Find the <vos> element within the gateway entry
					NodeList vosList = gatewayElement.getElementsByTagName("vos");
					for (int j = 0; j < vosList.getLength(); j++) {
						Node vosNode = vosList.item(j);
						if (vosNode.getNodeType() == Node.ELEMENT_NODE) {
							Element vosElement = (Element) vosNode;




							String voKey = vosElement.getElementsByTagName("key").item(0).getTextContent().trim();
							String voName = vosElement.getElementsByTagName("name").item(0).getTextContent().trim();
							String voScope = vosElement.getElementsByTagName("scope").item(0).getTextContent().trim();

							//getting vo
							Vos vos = new Vos(voKey,voScope, voName);


							// Find the <vres> element within the VO entry
							NodeList vresList = vosElement.getElementsByTagName("vres");
							for (int l = 0; l < vresList.getLength(); l++) {
								Node vresNode = vresList.item(l);
								if (vresNode.getNodeType() == Node.ELEMENT_NODE) {
									Element vresElement = (Element) vresNode;
									// Iterate through <entry> elements representing VREs within <vres>
									NodeList vreEntryList = vresElement.getElementsByTagName("entry");
									for (int m = 0; m < vreEntryList.getLength(); m++) {
										Node vreEntryNode = vreEntryList.item(m);
										if (vreEntryNode.getNodeType() == Node.ELEMENT_NODE) {
											Element vreEntryElement = (Element) vreEntryNode;
											String vreKey = vreEntryElement.getElementsByTagName("key").item(0)
													.getTextContent().trim();
											String vreName = vreEntryElement.getElementsByTagName("name")
													.item(0).getTextContent().trim();
											String vreDescription = vreEntryElement
													.getElementsByTagName("description").item(0)
													.getTextContent().trim();
											String vreManager = vreEntryElement.getElementsByTagName("managers")
													.item(0).getTextContent().trim();
											String vreStartDate = vreEntryElement
													.getElementsByTagName("startdate").item(0).getTextContent().trim();
											String vreEndDate = vreEntryElement.getElementsByTagName("enddate")
													.item(0).getTextContent().trim();

											// Create a new Vres object and add it to the Vos's vreList
											Vres vres = new Vres(vreKey, vreName, vreDescription, vreManager,
													vreStartDate, vreEndDate);
											vos.addVres(vres);


										}
									}
								}
							}

							// Add the Vos object to the gateway's vosList
							gateway.addVos(vos);
							//}
							//}
						}
					}

					// Store the gateway object in the gateways map

					gatewaysMap.put(gatewayName, gateway);
					//computeIfAbsent(gatewayName, k -> new ArrayList<>()).add(gateway);
				}
			}
			return gatewaysMap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public void getGatewaysInfo() {
		System.out.println("Name of the Gateway:");
		gatewaysMap.keySet().stream().filter(gatewayName -> gatewayName.endsWith("Gateway"))
		.forEach(System.out::println);
	}
	// Other methods...

	@Override
	public void update() {
		// Implement update logic if needed
	}

	@Override
	public void find() {
		// Implement find logic if needed
	}

	@Override
	public void read() {
		// Implement read logic if needed
	}

	@Override
	public void write() {
		// Implement write logic if needed
	}



	public void getVosInfo(String _gateway) {
		System.out.println("VOs Information for Gateway: " + _gateway);

		if (gatewaysMap.containsKey(_gateway)) {
			Gateway gatewayItem = gatewaysMap.get(_gateway);
			//Gateway gateway = gatewayList.get(0); // Get the first gateway with the specified name
			ArrayList<Vos> vosList = gatewayItem.getVosList();
			if (vosList != null) {
				for (Vos vos : vosList) {
					vos.printInfo();
					//System.out.println("Name of VREs: ");
					//ArrayList<Vres> vresList = vos.getVresList();
					//if (vresList != null && !vresList.isEmpty()) {
					//	for (Vres vre : vresList) {
					//		vre.printInfo();
					//	}
					//} else {
					//		System.out.println("No VREs found for this VOS.");
					//	}
					//	System.out.println("----------------------------------------");
				}
			} else {
				//System.out.println("No VOS found for this gateway.");
			}
		} else {
			System.out.println("No gateways found with the name: " + _gateway);
		}
	}

	// Method to find a VO within a gateway
	public void findVo(String _gateway, String _vo) {

		if (gatewaysMap.containsKey(_gateway)) 
		{
			Gateway gatewayItem = gatewaysMap.get(_gateway);
			boolean found = false;
			ArrayList<Vos> vosList = gatewayItem.getVosList();
			if (vosList != null) {
				for (Vos vos : vosList) {
					if (vos.getName().equals(_vo)) {
						found = true;
						System.out.println("VO info for " + _vo + " in " + _gateway + ":");
						System.out.println("Name of VOS: " + vos.getName());
					}
				}

			}
			if (!found) {
				System.out.println("VO " + _vo + " not found in the gateway " + _gateway + ".");
			}
		} else {
			System.out.println("Gateway " + _gateway + " not found.");
		}
	}

	public void findVre(String _gateway, String _vo, String _vre) {
		//System.out.println("VRE Information for VO: " + _vo + " in Gateway:\n " + _gateway);
		boolean vreFound = false; // Flag to track if VRE exists
		//ArrayList<Gateway> gatewayList = gatewaysMap.get(_gateway);

		if (gatewaysMap.containsKey(_gateway)) {

			Gateway gatewayItem = gatewaysMap.get(_gateway);
			ArrayList<Vos> vosList = gatewayItem.getVosList();
			if (vosList != null) {
				for (Vos vos : vosList) {
					if (vos.getName().equals(_vo)) {
						ArrayList<Vres> vresList = vos.getVresList();
						if (vresList != null) {
							for (Vres vre : vresList) {
								if (vre.getName().equals(_vre)) {
									// Print VRE information
									vre.printInfo();
									vreFound = true; // Set flag to true
									// return; // Exit method after printing information
								}
							}
						}
					}
				}
			}
		}
		// If execution reaches here and VRE not found, print message
		if (!vreFound) {
			System.out.println("VRE does not exist in the " + _vo + " of the " + _gateway);
		}
	}

	public HashMap<String, ArrayList<Gateway>> parseToHashMap() {
		// TODO Auto-generated method stub
		return null;
	}
	public void writeXml(HashMap<String, Gateway> map, String outputXmlFilePath) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();

		// Create root element <genericResources>
		Element genericResourcesElement = document.createElement("genericResources");
		document.appendChild(genericResourcesElement);

		// Create <Resource> element with version attribute
		Element resourceElement = document.createElement("Resource");
		resourceElement.setAttribute("version", "0.4.x");
		genericResourcesElement.appendChild(resourceElement);

		// Create <ID> element
		Element idElement = document.createElement("ID");
		idElement.appendChild(document.createTextNode("68322b8f-b916-44b9-9c93-bad8cceb9106"));
		resourceElement.appendChild(idElement);

		// Create <Type> element
		Element typeElement = document.createElement("Type");
		typeElement.appendChild(document.createTextNode("GenericResource"));
		resourceElement.appendChild(typeElement);

		// Create <Scopes> element
		Element scopesElement = document.createElement("Scopes");
		resourceElement.appendChild(scopesElement);

		// Create <Scope> element within <Scopes>
		Element scopeElement = document.createElement("Scope");
		scopeElement.appendChild(document.createTextNode("/d4science.research-infrastructures.eu"));
		scopesElement.appendChild(scopeElement);

		// Create <Profile> element
		Element profileElement = document.createElement("Profile");
		resourceElement.appendChild(profileElement);

		// Create <SecondaryType> element
		Element secondaryTypeElement = document.createElement("SecondaryType");
		secondaryTypeElement.appendChild(document.createTextNode("DetachedREs"));
		profileElement.appendChild(secondaryTypeElement);

		// Create <Name> element
		Element nameElement = document.createElement("Name");
		nameElement.appendChild(document.createTextNode("DetachedREsData"));
		profileElement.appendChild(nameElement);

		// Create <Description> element
		Element descriptionElement1 = document.createElement("Description");
		descriptionElement1.appendChild(document.createTextNode("Contains the information relating to dismissed Gateways, VOs and VREs."));
		profileElement.appendChild(descriptionElement1);

		// Create <Body> element
		Element bodyElement = document.createElement("Body");
		profileElement.appendChild(bodyElement);

		// Create <detachedres> element
		Element detachedResElement = document.createElement("detachedres");
		bodyElement.appendChild(detachedResElement);

		// Create <enabled> element within <detachedres>
		Element enabledElement = document.createElement("enabled");
		enabledElement.appendChild(document.createTextNode("true"));
		detachedResElement.appendChild(enabledElement);

		// Create <gateways> element within <detachedres>
		Element gatewaysElement = document.createElement("gateways");
		detachedResElement.appendChild(gatewaysElement);

		int gatewayKey = -1;

		// Iterate through the map entries to populate <entry> elements under <gateways>
		for (Map.Entry<String, Gateway> entry : map.entrySet()) {
			Gateway gateway = entry.getValue();

			// Create <entry> element
			Element entryElement = document.createElement("entry");
			gatewaysElement.appendChild(entryElement);

			// Create <key> element within <entry>
			Element keyElement = document.createElement("key");
			keyElement.appendChild(document.createTextNode(String.valueOf(gatewayKey)));
			entryElement.appendChild(keyElement);

			// Create <value> element within <entry>
			Element valueElement = document.createElement("value");
			entryElement.appendChild(valueElement);

			// Create <scope> element within <value>
			Element scopeValueElement = document.createElement("scope");
			scopeValueElement.appendChild(document.createTextNode(String.valueOf(gatewayKey)));
			valueElement.appendChild(scopeValueElement);

			// Create <name> element within <value>
			Element nameValueElement = document.createElement("name");
			String gatewayName = extractGatewayName(gateway.getName());
			nameValueElement.appendChild(document.createTextNode(gatewayName));
			valueElement.appendChild(nameValueElement);

			// Create <description> element within <value>
			Element descriptionElement = document.createElement("description");
			descriptionElement.appendChild(document.createTextNode("No description for gateway"));
			valueElement.appendChild(descriptionElement);

			Element vosElement = document.createElement("vos");
			valueElement.appendChild(vosElement);

			// Collect VREs under their respective VOs
			Map<String, Element> voElements = new HashMap<>();

			for (Vos vos : gateway.getVosList()) {
				Element vosEntryElement;
				Element vosValueElement;
				if (voElements.containsKey(vos.getName())) {
					vosEntryElement = voElements.get(vos.getName());
					vosValueElement = (Element) vosEntryElement.getElementsByTagName("value").item(0);
				} else {
					vosEntryElement = document.createElement("entry");
					vosElement.appendChild(vosEntryElement);

					Element vosKeyElement = document.createElement("key");
					vosKeyElement.appendChild(document.createTextNode("/d4science.research-infrastructures.eu/" + vos.getName()));
					vosEntryElement.appendChild(vosKeyElement);

					vosValueElement = document.createElement("value");
					vosEntryElement.appendChild(vosValueElement);

					Element vosScopeElement = document.createElement("scope");
					vosScopeElement.appendChild(document.createTextNode("/d4science.research-infrastructures.eu/" + vos.getName()));
					vosValueElement.appendChild(vosScopeElement);

					Element vosNameElement = document.createElement("name");
					vosNameElement.appendChild(document.createTextNode(vos.getName()));
					vosValueElement.appendChild(vosNameElement);

					voElements.put(vos.getName(), vosEntryElement);
				}

				Element vresElement = document.createElement("vres");
				vosValueElement.appendChild(vresElement);

				for (Vres vres : vos.getVresList()) {
					Element vresEntryElement = document.createElement("entry");
					vresElement.appendChild(vresEntryElement);

					Element vresKeyElement = document.createElement("key");
					vresKeyElement.appendChild(document.createTextNode("/d4science.research-infrastructures.eu/" + vos.getName() + "/" + vres.getName()));
					vresEntryElement.appendChild(vresKeyElement);

					Element vresValueElement = document.createElement("value");
					vresEntryElement.appendChild(vresValueElement);

					Element vreScopeElement = document.createElement("scope");
					vreScopeElement.appendChild(document.createTextNode("/d4science.research-infrastructures.eu/" + vos.getName() + "/" + vres.getName()));
					vresValueElement.appendChild(vreScopeElement);

					Element vresNameElement = document.createElement("name");
					vresNameElement.appendChild(document.createTextNode(vres.getName()));
					vresValueElement.appendChild(vresNameElement);

					Element vresDescriptionElement = document.createElement("description");
					vresDescriptionElement.appendChild(document.createTextNode(vres.getDescription()));
					vresValueElement.appendChild(vresDescriptionElement);

					Element vresManagerElement = document.createElement("managers");
					vresManagerElement.appendChild(document.createTextNode(vres.getManager()));
					vresValueElement.appendChild(vresManagerElement);

					Element vresStartDateElement = document.createElement("startdate");
					vresStartDateElement.appendChild(document.createTextNode(vres.getStartDate()));
					vresValueElement.appendChild(vresStartDateElement);

					Element vresEndDateElement = document.createElement("enddate");
					vresEndDateElement.appendChild(document.createTextNode(vres.getEndDate()));
					vresValueElement.appendChild(vresEndDateElement);
				}
			}
			gatewayKey--;
		}

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(new File(outputXmlFilePath));
		transformer.transform(source, result);
	}
	// Other methods...
	public String extractGatewayName(String gateways) {
		if (gateways.startsWith("https://")) {
			return gateways.substring(8, gateways.indexOf("."));
		} else if (gateways.startsWith("http://")) {
			return gateways.substring(7, gateways.indexOf("."));
		} else {
			return gateways; // fallback
		}
	}

}

