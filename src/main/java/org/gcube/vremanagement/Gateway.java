package org.gcube.vremanagement;

import java.util.ArrayList;

public class Gateway {
	private String name;
	private String description;
	private ArrayList<Vos>listOfVos;
	public Gateway(String name, String description) {
		this.name = name;
		this.description = description;
		this.listOfVos= new ArrayList<Vos>();
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	// Method to print gateway information
	public void printInfo() {
		System.out.println("Gateway Name: " + name);
		System.out.println("Gateway Description: " + description);
	}

	public ArrayList<Vos> getVosList() {
		// TODO Auto-generated method stub
		return listOfVos;
	}

	// In the Gateway class
	public void addVos(Vos vos) {
		listOfVos.add(vos);
	}
	public Vos getVosByName(String name) {
		for (Vos vos : listOfVos) {
			if (vos.getName().equals(name)) {
				return vos;
			}
		}
		return null;
	} 
}
