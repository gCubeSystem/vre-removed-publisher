package org.gcube.vremanagement;

import java.util.ArrayList;

public class Vres {
	private String key;
	private String name;
	private String description;
	private String manager;
	private String startDate;
	private String endDate;

	public Vres(String key, String name, String description, String manager, String startDate, String endDate) {
		this.key = key;
		this.name = name;
		this.description = description;
		this.manager = manager;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Vres() {
		// TODO Auto-generated constructor stub
	}

	public Vres(String string, String vreNames) {
		// TODO Auto-generated constructor stub
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getManager() {
		return manager;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	// Method to print VRE information
	public void printInfo() {
		System.out.println("VRE Key: " + key);
		System.out.println("VRE Name: " + name);
		System.out.println("VRE Description: " + description);
		System.out.println("VRE Manager: " + manager);
		System.out.println("VRE Start Date: " + startDate);
		System.out.println("VRE End Date: " + endDate);
	}

	public String getManagers() {
		// TODO Auto-generated method stub
		return manager;
	}

	public String getCatalogUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDescription(String description2) {
		// TODO Auto-generated method stub

	}

	public void setManagers(ArrayList<String> arrayList) {
		// TODO Auto-generated method stub

	}

	public void setStartDate(String startTime) {
		// TODO Auto-generated method stub

	}

	public void setEndDate(String endTime) {
		// TODO Auto-generated method stub

	}

}
