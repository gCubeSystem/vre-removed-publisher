package org.gcube.vremanagement;

import java.util.ArrayList;
import java.util.HashMap;

public class Vos {
	private String key;
	private String name;
	private String scope;
	private ArrayList<Vres>listOfVres;
	public Vos(String key,String scope,String name) {
		this.key = key;
		this.name = name;
		this.scope = scope;
		this.listOfVres=new ArrayList<Vres>();

	}


	public Vos(String string, String vo) {
		// TODO Auto-generated constructor stub
	}


	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

	public String getScope() {
		return scope;
	}

	// Method to print VO information
	public void printInfo() {
		System.out.println("VO Key: " + key);
		System.out.println("VO Name: " + name);
		System.out.println("VO Scope: " + scope);
		System.out.println("VRES Names:");
		if (!listOfVres.isEmpty()) {
			for (Vres vres : listOfVres) {
				System.out.println("VRES Name: " + vres.getName());
			}
		} else {
			System.out.println("No VRES found for this VO.");
		}
	}


	public ArrayList<Vres> getVresList() {
		// TODO Auto-generated method stub
		return listOfVres;
	}

	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	public void addVres(Vres vres) {
		// TODO Auto-generated method stub
		listOfVres.add(vres);
	}
	public Vres getVresByName(String name) {
		for (Vres vres : listOfVres) {
			if (vres.getName().equals(name)) {
				return vres;
			}
		}
		return null;
	} 
}
