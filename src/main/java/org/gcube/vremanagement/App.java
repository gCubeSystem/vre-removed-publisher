package org.gcube.vremanagement;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class App {

	public static void main(String[] args) {
		try {
			String csvFilePath = "src/main/resources/updated_VREDecommisioned-240326.csv";
			String xmlFilePath = "src/main/resources/doc.xml";


			// Initialize managers
			CsvManager csvManager = new CsvManager(csvFilePath);
			XmlManager xmlManager = new XmlManager(xmlFilePath);

			// Parse CSV and XML files
			HashMap<String, Gateway> xmlMap =  xmlManager.parse();
			HashMap<String, Gateway> csvMap =  csvManager.parseCsvToMap(csvFilePath);
			putAll(xmlMap,csvMap);


			//Merging csvMap into the xmlMap
			//Note that: in case of duplicate elements, the csvMap elements will overwrite the xmlMap
			//this is ok for us because the csv elements are mode recent than xml element
			//check this behaviour in the final xml doc 


			xmlManager.writeXml(xmlMap, "finalDoc.xml");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Methods to query and print information
		//xmlManager.getGatewaysInfo();
		//xmlManager.getVosInfo("D4Science.org Detached Gateway");
		//xmlManager.findVo("D4Science.org Detached Gateway", "gCubeApps");
		//xmlManager.findVre("D4Science.org Detached Gateway", "D4OS", "CNROutreach");
	}

	//This method checks duplicates and merge
	public static void putAll(HashMap<String, Gateway> xmlMap, HashMap<String, Gateway> csvMap) {
		for (String csvKey : csvMap.keySet()) {
			Gateway csvGateway = csvMap.get(csvKey);

			// Normalize the CSV gateway name
			String csvGatewayNameNormalized = csvGateway.getName().toLowerCase();
			XmlManager xmlManager = new XmlManager();
			String csvgatewayName = xmlManager.extractGatewayName(csvGatewayNameNormalized);
			// Check for duplicates by gateway name (case insensitive)
			boolean foundDuplicate = false;
			for (String xmlKey : xmlMap.keySet()) {
				Gateway xmlGateway = xmlMap.get(xmlKey);
				String xmlGatewayNameNormalized = xmlGateway.getName().toLowerCase();

				if (xmlGatewayNameNormalized.equalsIgnoreCase(csvgatewayName)) {
					// Merge VOs and VREs
					for (Vos csvVos : csvGateway.getVosList()) {
						Vos xmlVos = xmlGateway.getVosByName(csvVos.getName());
						if (xmlVos != null) {
							for (Vres csvVres : csvVos.getVresList()) {
								if (xmlVos.getVresByName(csvVres.getName()) == null) {
									xmlVos.addVres(csvVres);
								}
							}
						} else {
							xmlGateway.addVos(csvVos);
						}
					}
					foundDuplicate = true;
					break;
				}
			}

			// If no duplicate found, add the CSV gateway to the XML map
			if (!foundDuplicate) {
				xmlMap.put(csvKey, csvGateway);
			}
		}
	} 
}



